using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJ23 : MonoBehaviour
{

    ControlesJ2 controles;
    public GameObject MenuPausa;
    public bool patear;
    private float speed = 5;
    public Rigidbody rb;
    private int salto = 0;
    private int maximoDeSaltos = 1;
    private float fuerza = 7f;
    public bool detectaPiso;
    private float moveInput;
    private float kickForce = 7f;
    private float kickForceLeft = 4f;
    private bool isPowerUpActive = false;
    private float powerUpTimer = 0f;
    private float powerUpDuration = 3f;
    private float originalKickForce;
    private float originalKickForceLeft;
    private float originalFuerza;
    private Vector3 originalScale;
    private float originalSpeed;
    public bool congelado;
    public Material firstMaterial;
    public Material newMaterial;
    public GameObject ball;
    public GameObject arco;
    public GameObject arcoPropio;
    public ParticleSystem particleSystem;
    public List<string> tagsDeseados = new List<string>();

    private void Awake()
    {
        controles = new ControlesJ2();
        controles.Juego2.saltar.performed += ctx => Salto();
        controles.Juego2.Moveraizquierda.performed += ctx => moveInput -= 1f;
        controles.Juego2.MoverDerecha.performed += ctx => moveInput += 1f;
        controles.Juego2.Moveraizquierda.canceled += ctx => moveInput += 1f;
        controles.Juego2.MoverDerecha.canceled += ctx => moveInput -= 1f;
        controles.Juego2.patear.performed += ctx => Patear();
        controles.Juego2.patear.canceled += ctx => DejarDePatear();

    }

    private void Update()
    {
        Mover();
        if (isPowerUpActive)
        {
            powerUpTimer += Time.deltaTime;

            if (powerUpTimer >= powerUpDuration)
            {

                ResetPowerUp();
            }
        }
    }
    void Start()
    {
        originalKickForce = kickForce;
        originalKickForceLeft = kickForceLeft;
        originalFuerza = fuerza;
        originalScale = transform.localScale;
        originalSpeed = speed;
        particleSystem.Stop();

    }


    void Salto()
    {
        if (salto > 0)
        {
            rb.AddForce(Vector3.up * fuerza, ForceMode.Impulse);
            salto -= 1;
        }
    }
    void Patear()
    {
        gameObject.transform.localScale = new Vector3(1.6f, 2, 1);
        patear = true;
    }
    void Reset()
    {
        SceneManager.LoadScene(3);
    }
    void DejarDePatear()
    {
        gameObject.transform.localScale = new Vector3(1, 2, 1);
        patear = false;
    }
    void Mover()
    {
        rb.velocity = new Vector3(moveInput * speed, rb.velocity.y, 0);
    }

    private void OnEnable()
    {
        controles.Juego2.Enable();
    }

    private void OnDisable()
    {
        controles?.Juego2.Disable();
    }
    void Pausar()
    {
        Time.timeScale = 0f;

        MenuPausa.SetActive(true);
    }
    void DejarPausa()
    {
        Time.timeScale = 1;

        MenuPausa.SetActive(false);
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (tagsDeseados.Contains(collision.gameObject.tag))
        {
            detectaPiso = true;
            salto = maximoDeSaltos;


        }
        if (patear == true)
        {
            if (collision.gameObject.CompareTag("Pelota"))
            {
                Rigidbody pelotaRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                if (pelotaRigidbody != null)
                {
                    pelotaRigidbody.AddForce(Vector3.up * kickForce, ForceMode.Impulse);
                    pelotaRigidbody.AddForce(Vector3.left * kickForceLeft, ForceMode.Impulse);
                }
            }
        }
        if (collision.gameObject.tag == "PUFuerza")
        {
            ActivatePowerUp();
            kickForce = kickForce * 1.5f;
            kickForceLeft = kickForceLeft * 1.5f;
            Renderer ballRenderer = ball.GetComponent<Renderer>();
            ballRenderer.material = newMaterial;

        }
        if (collision.gameObject.tag == "PUSalto")
        {
            ActivatePowerUp();
            fuerza = fuerza * 1.2f;

        }
        if (collision.gameObject.tag == "PUTamano")
        {
            ActivatePowerUp();
            gameObject.transform.localScale = new Vector3(1, 3, 1);

        }
        if (collision.gameObject.tag == "PUVelocidad")
        {
            ActivatePowerUp();
            speed = speed * 1.5f;
            particleSystem.Play();

        }
        if (collision.gameObject.tag == "Botella")
        {
            ActivatePowerUp();
            congelado = true;
            Congelado();
        }
        if (collision.gameObject.tag == "PUArco")
        {
            ActivatePowerUp();
            Transform Arco = arco.GetComponent<Transform>();
            Arco.transform.localScale = new Vector3(1.44f, 4, 2);

        }
        if (collision.gameObject.tag == "PUPelota")
        {
            ActivatePowerUp();
            ball.transform.localScale = new Vector3(2, 2, 2);
        }
        if (collision.gameObject.tag == "PUArcoPropio")
        {
            ActivatePowerUp();

            arcoPropio.transform.localScale = new Vector3(1.44f, 1, 2);
        }
    }
    void ActivatePowerUp()
    {
        isPowerUpActive = true;
        powerUpTimer = 0f;
    }

    void ResetPowerUp()
    {
        isPowerUpActive = false;
        powerUpTimer = 0f;

        kickForce = originalKickForce;
        kickForceLeft = originalKickForceLeft;
        fuerza = originalFuerza;
        gameObject.transform.localScale = originalScale;
        speed = originalSpeed;
        congelado = false;
        Renderer ballRenderer = ball.GetComponent<Renderer>();
        ballRenderer.material = firstMaterial;
        arco.transform.localScale = new Vector3(1.44f, 2, 2);
        ball.transform.localScale = new Vector3(1, 1, 1);
        arcoPropio.transform.localScale = new Vector3(1.44f, 2, 2);
        particleSystem.Stop();
        Congelado();
    }
    void Congelado()
    {
        if (congelado)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Renderer>().material.color = new Color(0.5f, 0.8f, 1f);
        }
        else if (congelado == false)
        {
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Renderer>().material.color = Color.blue;
        }

    }
}


