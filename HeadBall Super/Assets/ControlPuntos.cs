using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ControlPuntos : MonoBehaviour
{
    public TMP_Text puntos;
    private int pPuntos = 0;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Pelota"))
        {
            pPuntos++;
            ActualizarContadorTexto1();
        }
        
    }
    private void ActualizarContadorTexto1()
    {
        puntos.text = "" + pPuntos.ToString();
    }
}
