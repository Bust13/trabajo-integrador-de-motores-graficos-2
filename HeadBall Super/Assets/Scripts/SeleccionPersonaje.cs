using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SeleccionPersonaje : MonoBehaviour
{
    // Start is called before the first frame update
    public bool Pj1;
    public bool Pj2;
    public bool Pj3;
    public bool Pj4;
    public bool Pj5;
    public bool Pj6;

    private void Update()
    {
        Pj1 = PlayerPrefs.GetInt("Pj1Select") == 11;
        Pj2 = PlayerPrefs.GetInt("Pj2Select") == 11;
        Pj3 = PlayerPrefs.GetInt("Pj3Select") == 11;
        Pj4 = PlayerPrefs.GetInt("Pj4Select") == 11;
        Pj5 = PlayerPrefs.GetInt("Pj5Select") == 11;
        Pj6 = PlayerPrefs.GetInt("Pj6Select") == 11;
    }
    public void Personaje1()
    {
        Pj1 = true;
        Pj2 = false;
        Pj3 = false;
        Guardar();
        SceneManager.LoadScene(2);
    }
    public void Personaje2()
    {
        Pj1 = false;
        Pj2 = true;
        Pj3 = false;
        Guardar();
        SceneManager.LoadScene(2);
    }
    public void Personaje3()
    {
        Pj1 = false;
        Pj2 = false;
        Pj3 = true;
        Guardar();
        SceneManager.LoadScene(2);
    }
    public void Personaje4()
    {
        Pj4 = true;
        Pj5 = false;
        Pj6 = false;
        Guardar();
        SceneManager.LoadScene(3);


    }
    public void Personaje5()
    {
        Pj4 = false;
        Pj5 = true;
        Pj6 = false;
        Guardar();
        SceneManager.LoadScene(3);
    }
    public void Personaje6()
    {
        Pj4 = false;
        Pj5 = false;
        Pj6 = true;
        Guardar();
        SceneManager.LoadScene(3);
    }
    public void Guardar() 
    {
        PlayerPrefs.SetInt("Pj1Select", Pj1 ? 11 : 0);
        PlayerPrefs.SetInt("Pj2Select", Pj2 ? 11 : 0);
        PlayerPrefs.SetInt("Pj3Select", Pj3 ? 11 : 0);
        PlayerPrefs.SetInt("Pj4Select", Pj4 ? 11 : 0);
        PlayerPrefs.SetInt("Pj5Select", Pj5 ? 11 : 0);
        PlayerPrefs.SetInt("Pj6Select", Pj6 ? 11 : 0);
    }
    public void SeleccionDePersonaje1()
    {
        SceneManager.LoadScene(1);
    }
    public void SelecciondDePersonaje2()
    {
        SceneManager.LoadScene(2);
    }
    public void Jugar()
    {
        SceneManager.LoadScene(3);
    }

    public void Escenario1()
    {
        SceneManager.LoadScene(4);
    }

    public void Escenario2()
    {
        SceneManager.LoadScene(5);
    }
    public void Escenario3()
    {
        SceneManager.LoadScene(6);
    }

    public void EscenarioFavela()
    {
        SceneManager.LoadScene(7);
    }
   public void PracticaLibre()
    {
        SceneManager.LoadScene("Practica");
    }
    public void PracticaEscenario()
    {
        SceneManager.LoadScene(11);
    }
    public void Salir()
    {
        Application.Quit();
    }
}
