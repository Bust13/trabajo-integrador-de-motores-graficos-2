using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesaparecerImagenCarga : MonoBehaviour
{
    public GameObject image; 
    public float displayTime = 3f; 

    private float timer; 
    private void Start()
    {
       
        timer = 0f;
    }

    private void Update()
    {
        
        timer += Time.deltaTime;

        if (timer >= displayTime)
        {
           
            image.SetActive(false);
            enabled = false;
        }
    }
}


