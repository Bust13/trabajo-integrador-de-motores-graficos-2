using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarOvni : MonoBehaviour
{
    public Transform pos;
    public GameObject Ovni;
    private float minTime = 10f;
    private float maxTime = 13f;

    private void Start()
    {
        InvokeRepeating("InstanciarObjeto", RandomTime(), RandomTime());
    }

    private float RandomTime()
    {
        return Random.Range(minTime, maxTime);
    }

    private void InstanciarObjeto()
    {
        Instantiate(Ovni, pos.position, pos.rotation);
    }
}



