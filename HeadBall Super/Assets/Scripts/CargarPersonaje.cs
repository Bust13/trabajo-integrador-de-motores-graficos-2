using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargarPersonaje : MonoBehaviour
{
    public GameObject pj1;
    public GameObject pj2;
    public GameObject pj3;
    public GameObject pj4;
    public GameObject pj5;
    public GameObject pj6;
    public bool Pj1;
    public bool Pj2;
    public bool Pj3;
    public bool Pj4;
    public bool Pj5;
    public bool Pj6;
    private void Update()
    {
        Pj1 = PlayerPrefs.GetInt("Pj1Select") == 11;
        Pj2 = PlayerPrefs.GetInt("Pj2Select") == 11;
        Pj3 = PlayerPrefs.GetInt("Pj3Select") == 11;
        Pj4 = PlayerPrefs.GetInt("Pj4Select") == 11;
        Pj5 = PlayerPrefs.GetInt("Pj5select") == 11;
        Pj6 = PlayerPrefs.GetInt("Pj6Select") == 11;

        if (Pj1 == true)
        {
            pj1.SetActive(true);
            Destroy(pj2); 
            Destroy(pj3);
        }
        if (Pj2 == true)
        {
            pj2.SetActive(true);
            Destroy(pj1);
            Destroy(pj3);
        }
        if (Pj3 == true)
        {
            pj3.SetActive(true);
            Destroy(pj1);
            Destroy(pj2);
        }
        if (Pj4 == true)
        {
            pj4.SetActive(true);
            Destroy(pj5);
            Destroy(pj6);
        }
        if (Pj5 == true)
        {
            pj5.SetActive(true);
            Destroy(pj4);
            Destroy(pj6);
        }
        if (Pj6 == true)
        {
            pj6.SetActive(true);
            Destroy(pj4);
            Destroy(pj5);
        }
    }
}
