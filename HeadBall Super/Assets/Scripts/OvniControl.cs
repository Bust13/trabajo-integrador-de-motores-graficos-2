using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvniControl : MonoBehaviour
{

    public float abduccionFuerza = 10f; // Fuerza de abducci�n ajustable
    public float abductionDuracion = 100f; // Duraci�n de la abducci�n
    public float abductionAltura = 5f; // Altura m�xima de la abducci�n
    public List<string> targetTags;

    private float abductionTiempo = 0f;
    private bool abducido = false;
  
    private void Update()
    {
        if (abducido)
        {
            abductionTiempo += Time.deltaTime;

            if (abductionTiempo >= abductionDuracion)
            {
                abducido = false;
                abductionTiempo = 0f;
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (targetTags.Contains(other.tag) && !abducido)
        {
            Rigidbody playerRigidbody = other.GetComponent<Rigidbody>();

            if (playerRigidbody != null)
            {
               
                Vector3 abductionDirection = transform.up;
 
                float abductionForceAdjusted = abduccionFuerza * (abductionAltura / Vector3.Distance(transform.position, other.transform.position));

                playerRigidbody.AddForce(abductionDirection * abductionForceAdjusted, ForceMode.Impulse);
              
                abducido = true;
            }
           
        }
    }
}






