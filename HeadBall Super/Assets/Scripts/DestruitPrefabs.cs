using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruitPrefabs : MonoBehaviour
{
    public float duracion = 3f;
    
    void Start()
    {
        
        StartCoroutine(DestruirDespuesDeTiempo());
    }
    
    IEnumerator DestruirDespuesDeTiempo()
    {
        
        yield return new WaitForSeconds(duracion);

      
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag=="Jugador")
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Jugador2")
        {
            Destroy(gameObject);
        }
    }
}
