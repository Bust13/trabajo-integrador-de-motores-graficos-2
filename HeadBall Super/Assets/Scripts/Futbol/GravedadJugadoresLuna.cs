using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravedadJugadoresLuna : MonoBehaviour
{
    private float fuerzaGravedad = 0.3f;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Vector3 antiGravity = -Physics.gravity * fuerzaGravedad;
        rb.AddForce(antiGravity, ForceMode.Acceleration);
    }
}
