using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador2Player2 : MonoBehaviour
{
    public bool patear;
    private float speed = 4;
    public Rigidbody rb;
    private int salto = 0;
    private int maximoDeSaltos = 1;
    private float fuerza = 9f;
    public bool detectaPiso;
  
    private float kickForce = 5f;
    private float kickForceLeft = 3f;
    private bool isPowerUpActive = false;
    private float powerUpTimer = 0f;
    private float powerUpDuration = 3f;
    private float originalKickForce;
    private float originalKickForceLeft;
    private float originalFuerza;
    private Vector3 originalScale;
    private float originalSpeed;
    public bool congelado;
    public Material fistMaterial;
    public Material newMaterial;
    public GameObject ball;
    public GameObject arco;
    public GameObject arcoPropio;
    public ParticleSystem particleSystem;
    public List<string> tagsDeseados = new List<string>();

    void Start()
    {
        originalKickForce = kickForce;
        originalKickForceLeft = kickForceLeft;
        originalFuerza = fuerza;
        originalScale = transform.localScale;
        originalSpeed = speed;
        particleSystem.Stop();

    }
    void Update()
    {
        Movement();
        Salto();
        Patear();
        if (isPowerUpActive)
        {
            powerUpTimer += Time.deltaTime;  

            if (powerUpTimer >= powerUpDuration)
            {
               
                ResetPowerUp();
            }
        }
    }
    private void Movement()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector3(1 * -speed, rb.velocity.y, 0);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }

    }
    void Patear()
    {

        if (Input.GetKeyDown(KeyCode.M))
        {
            gameObject.transform.localScale = new Vector3(1.6f, 2, 1);
            patear = true;
        }
        if (Input.GetKeyUp(KeyCode.M))
        {
            gameObject.transform.localScale = new Vector3(1, 2, 1);
            patear = false;
        }

    }
    void Salto()
    {
        if (Input.GetKey(KeyCode.UpArrow) && salto > 0)
        {
            rb.AddForce(Vector3.up * fuerza, ForceMode.Impulse);
            salto -= 1;

        }
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (tagsDeseados.Contains(collision.gameObject.tag))
        {
            detectaPiso = true;
            salto = maximoDeSaltos;


        }
        if (patear == true)
        {
            if (collision.gameObject.CompareTag("Pelota"))
            {
                Rigidbody pelotaRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                if (pelotaRigidbody != null)
                {
                    pelotaRigidbody.AddForce(Vector3.up * kickForce, ForceMode.Impulse);
                    pelotaRigidbody.AddForce(Vector3.left * kickForceLeft, ForceMode.Impulse);
                }
            }
        }
        if (collision.gameObject.tag == "PUFuerza")
        {
            ActivatePowerUp();
            kickForce = kickForce * 1.5f;
            kickForceLeft = kickForceLeft * 1.5f;
            Renderer ballRenderer = ball.GetComponent<Renderer>();
            ballRenderer.material = newMaterial;
            
        }
        if (collision.gameObject.tag == "PUSalto")
        {
            ActivatePowerUp();
            fuerza = fuerza * 1.2f;

        }
        if (collision.gameObject.tag == "PUTamano")
        {
            ActivatePowerUp();
            gameObject.transform.localScale = new Vector3(1, 3, 1);
            
        }
        if (collision.gameObject.tag == "PUVelocidad")
        {
            ActivatePowerUp();
            speed = speed * 1.5f;
            particleSystem.Play();

        }
        if (collision.gameObject.tag == "Botella")
        {
            ActivatePowerUp();
            congelado = true;
            Congelado();
        }
        if (collision.gameObject.tag == "PUArco")
        {
            ActivatePowerUp();
            Transform Arco = arco.GetComponent<Transform>();
            Arco.transform.localScale = new Vector3(1.44f, 4, 2);

        }
        if (collision.gameObject.tag == "PUPelota")
        {
            ActivatePowerUp();
            ball.transform.localScale = new Vector3(2, 2, 2);
        }
        if (collision.gameObject.tag == "PUArcoPropio")
        {
            ActivatePowerUp();

            arcoPropio.transform.localScale = new Vector3(1.44f, 1, 2);
        }
    }
    void ActivatePowerUp()
    {
        isPowerUpActive = true;
        powerUpTimer = 0f;
    }

    void ResetPowerUp()
    {
        isPowerUpActive = false;
        powerUpTimer = 0f;

        kickForce = originalKickForce;
        kickForceLeft = originalKickForceLeft;
        fuerza = originalFuerza;
        gameObject.transform.localScale = originalScale;
        speed = originalSpeed;
        Renderer ballRenderer = ball.GetComponent<Renderer>();
        ballRenderer.material = fistMaterial;
        congelado = false;
        ball.transform.localScale = new Vector3(1, 1, 1);
        arco.transform.localScale = new Vector3(1.44f, 2, 2);
        arcoPropio.transform.localScale = new Vector3(1.44f, 2, 2);
        particleSystem.Stop();
        Congelado();
    }
    void Congelado()
    {
        if (congelado)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Renderer>().material.color = new Color(0.5f, 0.8f, 1f);
        }
        else
        {
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Renderer>().material.color = Color.green;
        }
    }
}
