using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;

public class ControlPelota : MonoBehaviour
{
   
    private int contador = 0;
    private int contador2 = 0;
    public float contadorTime = 0;
    public float GolesJ1;
    public float GolesJ2;
    public Text contadorText;
    public TMP_Text contadorGol1;
    public TMP_Text contadorGol2;
    private Vector3 posicionInicial;
    public Rigidbody body;
    void Start()
    {
        posicionInicial = transform.position;
        body.AddForce(0, 0, 0);
        contadorTime = 63;
        contadorText.text = contadorTime.ToString();
        contadorGol1.text = GolesJ1.ToString();
        contadorGol2.text = GolesJ2.ToString();
        ActualizarContadorTexto1();
        ActualizarContadorTexto2();
    }
    private void Update()
    {
        
            contadorTime -= Time.deltaTime;
        contadorText.text = contadorTime.ToString("f0");
        Ganador();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Gol1"))
        {
            body.AddForce(0, 0, 0);
            body.transform.localScale = new Vector3(1, 1, 1);
            transform.position = posicionInicial;

            contador2++;
            GolesJ2++;
            ActualizarContadorTexto2();
        }

        if (other.gameObject.CompareTag("Gol2"))
        {
            body.AddForce(0, 0, 0);
            body.transform.localScale = new Vector3(1, 1, 1);
            transform.position = posicionInicial;

            GolesJ1++;
            contador++;
            ActualizarContadorTexto1();
        }
    


}
    
    private void ActualizarContadorTexto1()
    {
        contadorGol1.text = "" + contador.ToString();
    }
    private void ActualizarContadorTexto2()
    {
        contadorGol2.text = "" + contador2.ToString();
    }
    private void Ganador()
    {
        if (contadorTime <= 0)
        {
            if (GolesJ1 > GolesJ2)
            {
                SceneManager.LoadScene(8);

            }
            if (GolesJ1 < GolesJ2)
            {
                SceneManager.LoadScene(9);

            }
            if (GolesJ1== GolesJ2)
            {
                contadorTime = 30;

            }


        }
    }
    
}
