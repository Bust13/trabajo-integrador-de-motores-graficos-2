using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvniMovimiento : MonoBehaviour
{
    public Transform puntoA; 
    public Transform puntoB; 
    public float velocidad = 1f; 

    private Transform objetivo; 

    private void Start()
    {
        
        objetivo = Random.Range(0, 2) == 0 ? puntoA : puntoB;
    }

    private void Update()
    {
       
        transform.position = Vector3.MoveTowards(transform.position, objetivo.position, velocidad * Time.deltaTime);

       
        if (transform.position == objetivo.position)
        {
            if (objetivo == puntoA)
            {
                objetivo = puntoB;
            }
            else
            {
                objetivo = puntoA;
            }
        }
    }
}


