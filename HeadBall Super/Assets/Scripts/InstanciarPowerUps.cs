using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarPowerUps : MonoBehaviour
{
    public Transform pos;
    public GameObject[] powerUps;
 private float minTime = 10f;
   private float maxTime =13f;

    void Start()
    {
       
        InvokeRepeating("InstanciarObjeto", RandomTime(), RandomTime());
    }

    private float RandomTime()
    {
        
        return Random.Range(minTime, maxTime);
    }

    private void InstanciarObjeto()
    {
        int n = Random.Range(0, powerUps.Length);
        float poxGeneracion = Random.Range(-6, 6);
        Instantiate(powerUps[n], new Vector3(poxGeneracion,8,-0.5f), powerUps[n].transform.rotation);
    }
}
