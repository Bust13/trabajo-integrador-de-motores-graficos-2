using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausaJuego : MonoBehaviour
{
    
public GameObject MenuPausa;
 public void Volver()
     {
        Time.timeScale = 1;

        MenuPausa.SetActive(false);
     }
 public void Menu()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
  public void Salir()
        {
            Application.Quit
                ();
        }

    }

