using UnityEngine;

public class InstanciarBotellas : MonoBehaviour
{
    public Transform pos;
    public GameObject botellas;
    private float minTime = 15f;
    private float maxTime = 25f;

    void Start()
    {
        
        InvokeRepeating("InstanciarObjeto", RandomTime(), RandomTime());
    }

    private float RandomTime()
    {
        
        return Random.Range(minTime, maxTime);
    }

    private void InstanciarObjeto()
    {
        float poxGeneracion = Random.Range(-6, 6);
        float poyGeneracion = Random.Range(3, 7);
        Instantiate(botellas,  new Vector3(poxGeneracion, poyGeneracion, -0.5f), botellas.transform.rotation);
    }
}
